# Multiagent_map_coverage_simulator
For use in UML STRONG project to model a search agent's competency and Social Value Orientation

This script simulates the results of multiple agents searching a square map of desired size. 
Agents can have different search speeds, and an adjacency matrix. 
The adjacency matrix decides how much a team member cares about the performance of other team members. An agent can benefit other agents by marking for them the cells it has searched. As a result, the other agents will not have to waste time exploring these cells. Alternatively, an agent may decide to remove the marks for the agents it cares negatively for, in which case the performance of these other agents might be negatively affected.  
After the search, the effective search speed for every agent is calculated and outputted to a csv file for analysis.



