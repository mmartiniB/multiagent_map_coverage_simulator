"""
environment for discrete multi-agent search simulations. Accepts a search grid object and 
a list of agents objects who have a choose_action method.
"""

import numpy as np
from search_grid import SearchGrid, Cell
from utils.record_vis import RecordVis
import uuid
import os


class MultiAgentSearchEnv:
    def __init__(self,
                 search_grid: SearchGrid,
                 agents,
                 vis=False):
        self.search_grid = search_grid
        self.agents = agents
        self.n_agents = len(agents)
        self.vis = vis

        self.agents_periods = self.get_agents_periods()

        self.record = {}
        self.counter = 0

        self.id = 0  # environment id

    def get_agents_periods(self):
        """
        store periods of agents who have a period attribute
        otherwise default to 1
        """
        periods = np.ones(self.n_agents)
        for agent_id, agent in enumerate(self.agents):
            try:
                periods[agent_id] = agent.period
            except AttributeError:
                pass
        return periods

    def update_grid(self, cell: Cell):
        if len(cell.visitors) == 1:
            self.search_grid.visited_cells += 1
            if self.search_grid.visited_cells == self.search_grid.num_cells:
                return True
        return False

    def search(self):
        """run a simulation"""
        self.counter = 0
        while True:

            # let the appropriate agents take actions
            agents_turns = self.counter % self.agents_periods
            searchers_ids = np.where(agents_turns == 0)[0]
            np.random.shuffle(searchers_ids)
            for agent_id in searchers_ids:
                cell, update = self.agents[agent_id].choose_action(
                    self.search_grid)
                info = (agent_id, cell.row, cell.col)

                # populate the record
                if self.vis:
                    try:
                        self.record[self.counter].append(info)
                    except KeyError:
                        self.record[self.counter] = [info]

                if update:
                    done = self.update_grid(cell)

                if done:
                    if self.vis:
                        self.vis_record()
                    return

            self.counter += 1

    def vis_record(self):
        path = os.path.join("videos", uuid.uuid1().hex)
        RecordVis().vis_record(self.record, self.search_grid.shape, path)

    def reset(self):
        self.search_grid.reset()
        self.record = {}
        self.counter = 0
        for agent in self.agents:
            agent.pos = None
