"""
generate a csv file that would include network statistics and a column for 
the network url. This csv can then be merged with the database csv to enable performance
analysis using network statistics
"""
import numpy as np
import networkx as nx
from itertools import product
import pandas as pd
import os

"""helper functions for generating network statistics"""


def get_n_nodes(network_path: str) -> int:
    """get number of nodes from npy file name"""
    mat = np.load(network_path)
    return mat.shape[0]


def get_matrix_from_url(url) -> np.array:
    return np.load(url)


def get_graph_from_matrix(matrix: np.array) -> nx.DiGraph:
    return nx.from_numpy_matrix(matrix, create_using=nx.DiGraph)


def get_nodes_in_degree(G: nx.DiGraph, n_nodes: int) -> np.array:
    """return the in-degree of each node in graph G"""
    in_degs = np.zeros(n_nodes)
    in_degs_view = G.in_degree()
    for idx, in_deg in in_degs_view:
        in_degs[idx] = in_deg
    return in_degs


def get_nodes_out_degree(G: nx.DiGraph, n_nodes: int) -> np.array:
    """return the out-degree of each node in graph G"""
    out_degs = np.zeros(n_nodes)
    out_degs_view = G.out_degree()
    for idx, out_deg in out_degs_view:
        out_degs[idx] = out_deg
    return out_degs


def get_nodes_centrality(G: nx.DiGraph, n_nodes) -> np.array:
    """return the centrality of each node in graph G"""
    centralities = np.zeros(n_nodes)
    centralities_dict = nx.degree_centrality(G)
    for idx, centrality in centralities_dict.items():
        centralities[idx] = centrality
    return centralities


def get_edges_weights(G: nx.DiGraph, n_nodes) -> np.array:
    """return the weights of all edges of G"""
    weights = np.zeros(int(n_nodes**2))
    edges = product(range(n_nodes), range(n_nodes))
    for idx, (i, j) in enumerate(edges):
        try:
            weights[idx] = G[i][j]["weight"]
        except KeyError:
            # edge does not exist
            weights[idx] = 0
    return weights


def get_network_stats(adj_matrix: np.array) -> np.array:
    G = get_graph_from_matrix(adj_matrix)
    n_nodes = G.number_of_nodes()
    n_edges = G.number_of_edges()
    in_degrees = get_nodes_in_degree(G, n_nodes)
    out_degrees = get_nodes_out_degree(G, n_nodes)
    centralities = get_nodes_centrality(G, n_nodes)
    weights = get_edges_weights(G, n_nodes)
    stats_row = np.concatenate(
        ([n_nodes, n_edges], in_degrees, out_degrees, centralities, weights))
    return stats_row


def get_header(n_agents):
    header = """
UMASS LOWELL - STRONG PROJECT
Relational Network Statistics
    """ + "\n\n"

    network_stats_names = ["network_url", "n_nodes", "n_edges"]
    nodes_stats = ["in", "out", "centr"]
    nodes_stats_names = [
        f"{stat}_{i}" for stat in nodes_stats for i in range(n_agents)]
    edge_stats = ["w"]  # weight
    edge_stats_names = [f"{stat}_{i}{j}" for stat in edge_stats for i, j in product(
        range(n_agents), range(n_agents))]
    cols = network_stats_names + nodes_stats_names + edge_stats_names
    header += ",".join(cols) + "\n"
    return header


def output_stats(network_path, out_path):
    network = np.load(network_path)
    n_nodes = network.shape[0]
    out_path = out_path.format(n_nodes)

    stats = np.concatenate(([network_path], get_network_stats(network)))

    if not os.path.exists(out_path):
        with open(out_path, "a") as out:
            out.write(get_header(n_nodes))

    df = pd.DataFrame([stats])
    df.to_csv(out_path, mode='a', index=False, header=False)
    return


if __name__ == '__main__':
    import argparse
    description = """
    output a csv file of network statistics, selected by folder or file
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--networks_path', type=str,
                        help="path to the npy network, or folder containing npy network(s)")
    args = parser.parse_args()

    networks_path = args.networks_path
    out_path = os.path.join(networks_path, "network_database_{0}.csv")
    if os.path.isfile(networks_path):
        output_stats(networks_path, args.out_path)
    else:
        # a directory is given
        for network_name in os.listdir(networks_path):
            if not network_name.endswith("npy"):
                continue

            network_path = os.path.join(networks_path, network_name)
            n_nodes = get_n_nodes(network_path)
            output_stats(network_path, out_path.format(n_nodes))
