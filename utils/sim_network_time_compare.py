from sim_csv_parser import *
import matplotlib.pyplot as plt
import seaborn as sns
import os
import uuid


class SimNetworkTimeCompare(SimCsvParser):
    """
    Given dictionaries of simulation csv file names and their networks,
    output a box plot comparing the search time of all networks.
    Input dict: [{file name 1: network type 1}, {file name 2: network type 2}, ...]
    """

    def __init__(self, files_dict):
        # {network 1: [file1, file4], network 2: [file2], ...}
        self.networks = self.get_networks_from_files_dict(files_dict)

    def get_networks_from_files_dict(self, files_dict):
        networks = dict()
        for file, network in files_dict.items():
            try:
                networks[network].append(file)
            except KeyError:
                networks[network] = [file]
        return networks

    def get_networks_dataframes(self):
        networks_dfs_time = dict()  # {network 1: df1, network 2: df2, ...}
        networks_dfs_pi = dict()
        for network, paths in self.networks.items():
            for path in paths:
                df = self.get_dataframe(path)
                cols = list(df.columns)
                df_time = df[cols[-1:]]
                df_time = df_time.rename(columns={"Search Time (s)": network})
                df_pi = df[cols[:-1]]
                try:
                    networks_dfs_time[network] = networks_dfs_time[network].append(
                        df_time)
                except KeyError:
                    networks_dfs_time[network] = df_time
                try:
                    networks_dfs_pi[network] = networks_dfs_pi[network].append(
                        df_pi)
                except KeyError:
                    networks_dfs_pi[network] = df_pi
        return networks_dfs_time, networks_dfs_pi

    def combine_dfs(self, networks_dfs, axis):
        cols = [df for df in networks_dfs.values()]
        return pd.concat(cols, axis=axis)

    def combine_dfs_pi(self, networks_dfs):
        cols = [df for df in networks_dfs.values()]
        return pd.concat(cols, axis=1)

    def main(self):
        """
        parse self.networks: read all files belonging to a network and combine all time columns into one column;
        rename the column to the name of the network.
        then append the resulting column to result df. result df contains time columns for all networks,
        and is used to draw the box plot
        """
        networks_dfs_time, networks_dfs_pi = self.get_networks_dataframes()
        df_time = self.combine_dfs(networks_dfs_time, axis=1)
        df_pi = self.combine_dfs(networks_dfs_pi, axis=0)
        return df_time, df_pi


# def plot_time(df_time, num_agents, map_size):
#     font_size = 40
#     sns.set(rc={'figure.figsize': (30, 20),
#                 'xtick.labelsize': font_size - 14,
#                 'ytick.labelsize': font_size - 14,
#                 })
#     sns.set_style("whitegrid")

#     x_label = "Relational Network"
#     y_label = "Search Time (s)"
#     title = f"{y_label} per {x_label}: {num_agents} Agents in a {map_size} x {map_size} map"

#     sns.violinplot(data=df_time, palette="Set3")  # .set(title=title)

#     plt.gca().set_ylim(bottom=0)
#     plt.title(title, {"size": font_size})
#     plt.xlabel(x_label, {"size": font_size - 10})
#     plt.ylabel(y_label, {"size": font_size - 10})
#     # plt.ylim(0, 10)
#     plt.tight_layout()

#     fig_name = str(uuid.uuid1()) + ".png"
#     plt.savefig(fig_name)
#     plt.clf()


# def plot_pi(df_pi, num_agents):
#     font_size = 40
#     sns.set(rc={'figure.figsize': (30, 20),
#                 'xtick.labelsize': font_size - 14,
#                 'ytick.labelsize': font_size - 14,
#                 })
#     sns.set_style("whitegrid")

#     x_label = "Agent"
#     y_label = "Effective Search Speed: Pi (sq/s)"
#     # title = f"{y_label} per {x_label}"

#     sns.boxplot(data=df_pi, palette="Set3")  # .set(title=title)

#     plt.gca().set_ylim(bottom=0)
#     # plt.title(title, {"size": font_size})
#     plt.xlabel(x_label, {"size": font_size - 10})
#     plt.ylabel(y_label, {"size": font_size - 10})
#     # plt.ylim(0, 15)
#     plt.tight_layout()

#     fig_name = str(uuid.uuid1()) + ".png"
#     plt.savefig(fig_name)
#     plt.clf()


def plot_time(df, title):
    font_size = 40
    sns.set(rc={'figure.figsize': (30, 20),
                'xtick.labelsize': font_size - 14,
                'ytick.labelsize': font_size - 14,
                })

    networks = list(df.columns)
    networks_count = len(networks)
    fig, axes = plt.subplots(
        nrows=1,
        ncols=networks_count,
        figsize=(30, 10),
        sharey=True,
    )
    plt.suptitle(title, fontsize=40)

    for i, (network, ax) in enumerate(zip(networks, axes)):
        sns.kdeplot(
            data=df,
            x=None,
            y=network,
            # kde kwargs
            bw_adjust=0.5,
            clip_on=False,
            fill=True,
            alpha=1,
            linewidth=1.5,
            ax=ax,
            color="lightslategray",
        )

        keep_variable_axis = i == 0
        _format_axis(
            ax,
            network,
            keep_variable_axis=keep_variable_axis,
        )

    plt.tight_layout()

    fig_name = str(uuid.uuid1()) + ".png"
    plt.savefig(fig_name)
    plt.clf()


def _format_axis(ax, network_name, keep_variable_axis=True):

    # Remove the axis lines
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    ax.set_xlabel(None)
    lim = ax.get_xlim()
    ax.set_xticks([(lim[0] + lim[1]) / 2])
    ax.set_xticklabels([network_name])
    if not keep_variable_axis:
        ax.get_yaxis().set_visible(False)
        ax.spines["left"].set_visible(False)
    else:
        ax.set_ylabel("Search Time (s)", fontsize=26)

    ax.grid()


if __name__ == "__main__":
    num_agents = 5
    map_size = 20
    path_base = f"/home/du18/repos/Multiagent_map_coverage_simulator/results/asymmetric"
    files_dict = {
        # f"low-s.csv": "Survivalist - FOV 1",
        # f"low-c.csv": "Communitarian - FOV 1",
        # f"high-s.csv": "Survivalist - FOV 40",
        # f"high-c.csv": "Communitarian - FOV 40",
        f"rand_t0.csv": "In-Degree 0",
        f"rand_t1.csv": "In-Degree 1",
        f"rand_t2.csv": "In-Degree 2",
        f"rand_t3.csv": "In-Degree 3",
        f"t4_a1.csv": "In-Degree 4",
    }
    files_dict = {os.path.join(
        path_base, file_name): network for file_name, network in files_dict.items()}
    df_time, df_pi = SimNetworkTimeCompare(files_dict).main()

    # title = f"Effect of Resource Constraints on Network Structure and SAR Performance -\n{num_agents} Identical Agents in a {map_size} x {map_size} map"
    title = f"Effect of Asymmetric Skills on Network Symmetry - Agent 1 is 4 times faster than others. \nAgents' out-degree is constrained to 1. \nAgent 1's in-degree is increased from 0 to 4"

    plot_time(df_time, title)
    # plot_pi(df_pi, num_agents)
