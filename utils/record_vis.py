"""
script to receive search record from multiagent_search and visualize it.
different number of agents can be given, the script generates a random color for each agent.
"""
import numpy as np
import cv2


class RecordVis:
    def vis_record(self, record, grid_shape, out_path):
        frames = self.get_frames(record, grid_shape)
        self.frames_to_vid(frames, out_path)

    def get_frames(self, record, grid_shape):
        frames = np.zeros((len(record), grid_shape[0],
                          grid_shape[1], 3), dtype=np.uint8)
        agents_colors = {}  # id: color
        for i, time_step in enumerate(sorted(record.keys())):
            for agent_id, row, col in record[time_step]:
                try:
                    agent_color = agents_colors[agent_id]
                except KeyError:
                    agent_color = np.random.randint(255, size=3)
                    agents_colors[agent_id] = agent_color

                frames[i, row, col, :] = agent_color
                if i == len(record) - 1:
                    break
                frames[i + 1:, row, col,
                       :] += np.uint8(0.1 * (255 - frames[i + 1, row, col, 0]))
        return frames

    def frames_to_vid(self, frames, out_path, pad=5):
        size = frames.shape[1:-1]
        fps = 10
        side_len = min(4001, size[0] * 200)
        out = cv2.VideoWriter(f'{out_path}.mp4', cv2.VideoWriter_fourcc(
            *'mp4v'), fps, (side_len, side_len))
        for frame in frames:
            frame = cv2.resize(frame, (side_len, side_len),
                               interpolation=cv2.INTER_AREA)
            out.write(frame)
        out.release()
