"""
create 2-edge networks where agents weights range from 0 to 1 in 0.1 increments
"""

import numpy as np
import os


def main(base_dir):
    n_agents = 2
    adj_matrix = np.eye(n_agents)
    for edge_i in np.linspace(0, 1, 11):
        adj_matrix[0, 1] = edge_i

        for edge_j in np.linspace(0, 1, 11):
            adj_matrix[1, 0] = edge_j
            adj_path = os.path.join(
                base_dir, f"{round(edge_i, 1)}_{round(edge_j, 1)}")
            np.save(adj_path, adj_matrix)


if __name__ == '__main__':
    import sys

    try:
        base_dir = sys.argv[1]
    except IndexError:
        raise Exception(
            "Please select an output base directory for the networks")

    main(base_dir)
