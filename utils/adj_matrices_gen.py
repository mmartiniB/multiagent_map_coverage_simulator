"""
Script to provide functions for generating all possible adj. matrices 
of N nodes and visualizing them.
"""
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from itertools import combinations, product
import os


def get_edges(n_nodes: int) -> list:
    """return a list of edge coordinates"""
    return list(product(range(n_nodes), repeat=2))


def clean_edges(verts: list) -> list:
    """delete self loops"""
    verts_new = []
    for vert in verts:
        if vert[0] != vert[1]:
            verts_new.append(vert)
    return verts_new


def get_n_matrices(n_nodes: int, max_combos=1) -> dict:
    """get a list of all networks of a given number of nodes"""
    template = np.eye(n_nodes)
    edges = clean_edges(get_edges(n_nodes))
    network_idx = 0
    testbed = {network_idx: template}

    for i in range(len(edges)):

        # get all combinations of edges from 1 to N (one per loop)
        edge_combos = list(combinations(edges, i + 1))

        for c, comb in enumerate(edge_combos, start=1):
            temp = template.copy()
            x, y = np.split(np.array(comb), [-1], axis=1)
            temp[(x, y)] = 1
            network_idx += 1
            testbed[network_idx] = temp
            if c == max_combos:
                # if nodes are identical, all combinations are equivalent?
                network_idx += len(edge_combos) - max_combos
                break
    return testbed


def save_graphs(graphs: dict, figsize=8, dotsize=800, base_dir=".") -> None:
    """save images for all requested graphs"""
    if not os.path.exists(base_dir):
        os.mkdir(base_dir)
    for idx, g in graphs.items():
        plt.figure(figsize=(figsize, figsize))
        nx.draw(g, node_size=dotsize, with_labels=True,
                pos=nx.spring_layout(g))
        out_path = os.path.join(base_dir, str(idx))
        plt.savefig(f"{out_path}.png")
        plt.close()


def save_n_matrices(matrices: dict, base_dir):
    """save all requested matrices"""
    if not os.path.exists(base_dir):
        os.mkdir(base_dir)
    for idx, mat in matrices.items():
        out_path = os.path.join(base_dir, str(idx))
        np.save(out_path, mat, allow_pickle=True)


def main(n_nodes, max_combos):
    base_dir = "/home/du18/repos/Multiagent_map_coverage_simulator/networks"
    base_dir = os.path.join(base_dir, f"{n_nodes}")

    matrices = get_n_matrices(n_nodes, max_combos)
    graphs = {idx: nx.from_numpy_matrix(
        mat, create_using=nx.DiGraph) for idx, mat in matrices.items()}
    save_n_matrices(matrices, base_dir)
    save_graphs(graphs, base_dir=base_dir)


if __name__ == '__main__':
    import sys

    try:
        n_nodes = int(sys.argv[1])
    except IndexError:
        raise Exception(
            "Please enter a number of nodes as a command line argument")

    try:
        max_combos = int(sys.argv[2])
    except IndexError:
        max_combos = 1

    main(n_nodes, max_combos)
