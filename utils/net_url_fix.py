import os
import pandas as pd
import sys
sys.path.append('/home/du18/repos/Multiagent_map_coverage_simulator')

"""script to fix the urls for network and map - when data comes from different computers,
the urls might look different, so this scripts fixes that"""

if __name__ == "__main__":
    header = """
UMASS LOWELL - STRONG PROJECT
MultiAgent Grid Coverage Simulator
    """ + "\n\n"
    n_agents = int(sys.argv[1])
    path = sys.argv[2]
    out_dir = os.path.dirname(path)
    filename = os.path.basename(path)
    out_path = os.path.join(out_dir, "new_" + filename)
    with open(out_path, "w") as file:
        file.write(header)

    df = pd.read_csv(path, skiprows=5)
    def l(p): return f"networks/{n_agents}/{os.path.basename(p)}"
    df["network_url"] = df["network_url"].apply(l)

    def l2(p): return f"maps/{os.path.basename(p)}"
    df["map_url"] = df["map_url"].apply(l2)
    df.to_csv(out_path, mode="a", index=False)
