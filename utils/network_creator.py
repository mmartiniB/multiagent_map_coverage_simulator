import numpy as np
from random import choice
import networkx as nx


class NetworkCreator:

    def create_survivalist(self, num_agents, rand=False, scale=1, offset=0):
        """
        Create a survivalist network given a number of agents.
        rand gives random values instead of 1's
        scale scales these values
        offset is added to the diagonal values
        """
        network = np.identity(num_agents)
        if rand:
            rand_scale = np.random.rand(num_agents, num_agents)
            network *= rand_scale
        network *= scale
        network[np.where(network != 0)] += offset
        return network

    def create_authoritarian_simple(self, num_agents, num_authorities, rand=False, scale=1, offset=0):
        """
        Create a simple authoritarian network given a number of agents and a list of authorities:
            authorities do not serve each other. an agent serves only one authority.
            servants division over authorities is random: one authority could get more servants
            every authority has to have at least one servant
        """
        network = self.create_survivalist(num_agents=num_agents, scale=scale, offset=offset)
        authorities = np.random.default_rng().choice(num_agents, size=num_authorities, replace=False)

        last_choice = None
        for i, row in enumerate(network):
            if i in authorities:
                # here, an authority does not have an authority
                continue
            while True:
                # get an authority to serve
                auth_idx = choice(range(num_authorities))
                if auth_idx != last_choice or num_authorities == 1:
                    break
            last_choice = auth_idx

            # fill in the value
            network[i, authorities[auth_idx]] = 1
            if rand:
                rand_scale = np.random.uniform(0, 1, 1)
                network[i, authorities[auth_idx]] *= rand_scale
            network[i, authorities[auth_idx]] *= scale
            network[i, authorities[auth_idx]] += offset
        return network

    def create_tribal_simple(self, num_agents, rand=False, scale=1, offset=0):
        """
        create a simple tribal network:
            each agent cares about one other agent only
            two agents can care about one agent. some agents no one cares for
        """
        network = self.create_survivalist(num_agents=num_agents, scale=scale, offset=offset)
        for i in range(network.shape[0]):
            while True:
                index = choice(range(num_agents))
                if index != i:
                    break
            network[i, index] = 1
            if rand:
                rand_scale = np.random.uniform(0, 1, 1)
                network[i, index] *= rand_scale
            network[i, index] *= scale
            network[i, index] += offset
        return network

    def create_tribal_ideal(self, num_agents, num_tribes, rand=False, scale=1, offset=0):
        """
        create an ideal tribal network:
            each agent cares about one other agent only, and is cared for by one agent only
        """
        network = self.create_survivalist(num_agents=num_agents, scale=scale, offset=offset)
        if num_agents % num_tribes:
            raise Exception(
                f"Please choose the number of agents to be multiple of {num_tribes}, or number of tribes divisible by {num_agents}"
            )

        for i in range(network.shape[0]):
            other_agent = (i + num_tribes) % num_agents
            network[i, other_agent] = 1
            if rand:
                rand_scale = np.random.uniform(0, 1, 1)
                network[i, other_agent] *= rand_scale
            network[i, other_agent] *= scale
            network[i, other_agent] += offset
        return network

    def create_authoritarian_layered_old(self, num_agents, num_authorities, rand=False, scale=1, offset=0):
        def array_w_sum(length, sum_):
            a = np.random.dirichlet(np.ones(length), size=1)
            a = np.around(a * sum_)[0]
            a[a == 0] += 1
            dif = np.sum(a) - sum_
            if dif < 0:
                a[np.argmin(a)] -= dif
            elif dif > 0:
                a[np.argmax(a)] -= dif
            return a.astype(np.int)

        network = self.create_survivalist(num_agents=num_agents, scale=scale, offset=offset)
        lengths = array_w_sum(length=num_authorities, sum_=num_agents - 1)
        start = 0
        for i, length in enumerate(lengths):
            network[start + 1: start + 1 + length, i] = 1
            start += length

        return network

    def create_authoritarian_ideal(self, branch_factor, num_layers, rand=False, scale=1, offset=0):
        """
        create an ideal authoritarian network with a known number of layers
        """
        def fix_tree(network, scale, offset):
            for j in range(1, network.shape[1]):
                network[: j, j] *= 0
                val = 1
                if rand:
                    val = np.random.uniform()
                val = val * scale + offset
                network[j, j] = val
            return network
        G = nx.balanced_tree(branch_factor, num_layers)
        network = nx.to_numpy_array(G)
        network = fix_tree(network, scale, offset)
        return network

    def create_communitarian(self, num_agents, rand=False, scale=1, offset=0):
        if rand:
            network = np.random.uniform(0, 1, (num_agents, num_agents))
            np.fill_diagonal(network, 1)
        else:
            network = np.ones((num_agents, num_agents))
        return network * scale + offset


if __name__ == "__main__":
    import argparse
    import uuid

    description = \
        """
    Create a Relation Network and return it as a numpy array.
    0: survivalist,
    1: simple tribal,
    2: ideal tribal,
    3: simple authoritarian,
    4: ideal authoritarian,
    5: communitarian
    """
    id_ = uuid.uuid1().hex
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--network_type', type=int, default=0,
                        help="select network index from the description above")
    parser.add_argument('--num_agents', type=int, default=40,
                        help='number of agents')
    parser.add_argument('--num_authorities', type=int, default=13,
                        help='number of authorities in an authoritarian network')
    parser.add_argument('--num_tribes', type=int, default=1,
                        help='number of tribes in an ideal tribal network')
    parser.add_argument('--branch_factor', type=int, default=3,
                        help='branching factor for in ideal authoritarian network')
    parser.add_argument('--num_layers', type=int, default=3,
                        help='number of layers in an ideal authoritarian network')
    parser.add_argument('--rand', type=int, default=0,
                        help='whether network weights are random between 0 and 1')
    parser.add_argument('--scale', type=int, default=1,
                        help='scale to multiply network weights by')
    parser.add_argument('--offset', type=int, default=0,
                        help='offset to add to network weights')
    parser.add_argument('--out_path', type=str, default=f"networks/{id_}",
                        help='offset to add to network weights')
    parser.add_argument('--visualize', type=int, default=0,
                        help='1 to generate an image of the adjacency matrix, and a .gexf graph')

    args = parser.parse_args()
    network_type = args.network_type
    num_agents = args.num_agents
    num_authorities = args.num_authorities
    num_tribes = args.num_tribes
    branch_factor = args.branch_factor
    num_layers = args.num_layers
    rand = args.rand != 0
    scale = args.scale
    offset = args.offset

    out_path = args.out_path
    visualize = args.visualize

    nc = NetworkCreator()
    methods = {
        0: lambda: nc.create_survivalist(num_agents, rand, scale, offset),
        1: lambda: nc.create_tribal_simple(num_agents, rand, scale, offset),
        2: lambda: nc.create_tribal_ideal(num_agents, num_tribes, rand, scale, offset),
        3: lambda: nc.create_authoritarian_simple(num_agents, num_authorities, rand, scale, offset),
        4: lambda: nc.create_authoritarian_ideal(branch_factor, num_layers, rand, scale, offset),
        5: lambda: nc.create_communitarian(num_agents, rand, scale, offset),
    }
    network = methods[network_type]()
    np.save(out_path, network)
    if visualize:
        import matplotlib.pyplot as plt
        import seaborn as sns
        plt.figure(figsize=(16, 12), dpi=100)
        ax = sns.heatmap(network, vmin=0, vmax=1, cmap="BuPu")
        plt.savefig(out_path + ".png")
        G = nx.DiGraph(network)
        nx.write_gexf(G, out_path + ".gexf")
