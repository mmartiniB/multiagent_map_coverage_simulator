import pandas as pd

class SimCsvParser:

    def get_first_line(self, csv_path):
        with open(csv_path, "r") as file:
            for idx, line in enumerate(file.readlines()):
                if line.startswith("Pi_0"):
                    return idx

    def get_dataframe(self, csv_path):
        line_start = self.get_first_line(csv_path)
        return pd.read_csv(csv_path, skiprows=line_start)
