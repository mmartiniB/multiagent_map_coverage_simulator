"""
utility functions to help output the csv file after a simulation is done
"""

import pandas as pd
import numpy as np
import os


def get_header(num_agents):
    header = """
UMASS LOWELL - STRONG PROJECT
MultiAgent Grid Coverage Simulator
    """ + "\n\n"

    pi_cols = [f"pi_{i}" for i in range(num_agents)]
    types_cols = [f"algo_{i}" for i in range(num_agents)]
    fov_cols = [f"fov_{i}" for i in range(num_agents)]
    period_cols = [f"period_{i}" for i in range(num_agents)]

    cols = ["env"] + pi_cols + types_cols + fov_cols + period_cols
    cols += ["network_url", "map_url", "time"]

    header += ",".join(cols) + "\n"
    return header


def get_pi(env) -> np.array:
    """get the performance of agents"""
    pi = np.zeros(env.n_agents)
    for row in env.search_grid.grid:
        for cell in row:
            try:
                pi[cell.visitors[0]] += 1
            except AttributeError:
                # wall - no cell object
                pass
    return np.around(pi / env.counter, 2)


def get_result_row(env, agents_types, fovs, periods, adj_mat_url, map_url):
    """get the round results as a row"""
    result_pi = get_pi(env)
    row = np.concatenate(
        ([env.id], result_pi, agents_types, fovs, periods, [adj_mat_url, map_url, env.counter]))
    return row


def output_results(results: np.array, n_agents, out_path):
    if not os.path.exists(out_path):
        with open(out_path, "a") as out:
            out.write(get_header(n_agents))

    df = pd.DataFrame(results)
    df.to_csv(out_path, mode='a', index=False, header=False)
