import numpy as np
from search_grid import SearchGrid
from agents import GRW
from multiagent_search_env import MultiAgentSearchEnv
from utils.simulation_util import get_result_row, output_results
from tqdm import tqdm


if __name__ == '__main__':
    import argparse
    import uuid

    description = """
    simulate a multiagent grid coverage using an input numpy adjacency matrix.
    get simulation results as a csv file for all agents' performance and final search time
    """
    id_ = uuid.uuid1().hex
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--agents_types', type=int, default=(0,), nargs="+",
                        help="agents' algorithms - 0: Guided Random Walk")
    parser.add_argument('--env_type', type=int, default=0,
                        help="type of environment - 0: MultiAgentSearch")
    parser.add_argument('--num_agents', type=int,
                        help="number of agents")
    parser.add_argument('--fovs', type=int, default=(1,), nargs="+",
                        help="agents' fields of view in Manhattan distance")
    parser.add_argument('--adj_matrix', type=str,
                        help="path to .npy square adjacency matrix")
    parser.add_argument('--agents_periods', type=int, nargs="+", default=(1,),
                        help="tuple of agents periods (timesteps/cell)")
    parser.add_argument('--grid_path', type=str,
                        help='path to image or npy search map')
    parser.add_argument('--num_rounds', type=int, default=1,
                        help='number of rounds played')
    parser.add_argument('--out_path', type=str, default=f"results/{id_}.csv",
                        help='path of output csv file')
    parser.add_argument('--vis', type=int, default=0,
                        help='1 to generate a video of the search')

    args = parser.parse_args()

    # check for adj matrix
    if args.adj_matrix is not None:
        adj_matrix = np.load(args.adj_matrix)
    else:
        adj_matrix = None

    # handle number of agents
    num_agents = args.num_agents
    if num_agents is None:
        if args.adj_matrix is None:
            raise Exception(
                "Insifficient Arguments: number of agents not specified")
        num_agents = adj_matrix.shape[0]

    # get agents types (indexes)
    agents_types = list(args.agents_types)
    if len(agents_types) != args.num_agents:
        agents_types = np.random.choice(agents_types, size=(num_agents,))

    # handle agents periods - default to same speeds
    agents_periods = list(args.agents_periods)
    if len(agents_periods) != args.num_agents:
        agents_periods = np.random.choice(
            agents_periods, size=(num_agents,))

    # handle agents' fovs
    fovs = list(args.fovs)
    if len(fovs) != args.num_agents:
        fovs = np.random.choice(fovs, size=(num_agents,))

    # handle search grid
    search_grid = SearchGrid(args.grid_path)

    # prepare agents
    AGENTS_CLASSES = [
        GRW,  # id_: int, period: int, rel_row:np.array, fov: int,
    ]
    AGENTS_ARGS = [
        lambda i: (i, agents_periods[i], adj_matrix[i], fovs[i]),
    ]
    agents = [AGENTS_CLASSES[agent_type](*AGENTS_ARGS[agent_type](
        i)) for i, agent_type in enumerate(agents_types)]

    # prepare envirnoment
    ENVS = [
        MultiAgentSearchEnv,  # search_grid: np.array, agents: obj, vis: bool
    ]
    ENVS_ARGS = [
        (search_grid, agents, args.vis),
    ]
    env = ENVS[args.env_type](*ENVS_ARGS[args.env_type])

    # run the search rounds
    results = []
    for round_idx in tqdm(range(args.num_rounds)):
        env.reset()
        env.search()

        result_row = get_result_row(
            env, agents_types, fovs, agents_periods, args.adj_matrix, args.grid_path)
        results.append(result_row)
    output_results(results, num_agents, args.out_path)
