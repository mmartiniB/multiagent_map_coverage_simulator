class RelationalMatrix_2():
    SURVIVALIST = [
        [1, 0],
        [0, 1],
    ]

    AUTHORITARIAN_0 = [
        [1, 0],
        [1, 1],
    ]

    AUTHORITARIAN_0_COLLAPSED = [
        [1, 0],
        [1, 0],
    ]

    AUTHORITARIAN_1 = [
        [1, 1],
        [0, 1],
    ]

    AUTHORITARIAN_1_COLLAPSED = [
        [0, 1],
        [0, 1],
    ]

    TRIBAL_0 = [
        [1, 1],
        [1, 1],
    ]

    COMMUNITARIAN = [
        [1, 1],
        [1, 1],
    ]


class RelationalMatrix_3:
    SURVIVALIST = [
        [1, 0, 0],
        [0, 1, 0],
        [0, 0, 1],
    ]

    AUTHORITARIAN_0 = [
        [1, 0, 0],
        [1, 1, 0],
        [1, 0, 1],
    ]

    AUTHORITARIAN_0_COLLAPSED = [
        [1, 0, 0],
        [1, 0, 0],
        [1, 0, 0],
    ]

    AUTHORITARIAN_1 = [
        [1, 1, 0],
        [0, 1, 0],
        [0, 1, 1],
    ]

    AUTHORITARIAN_1_COLLAPSED = [
        [0, 1, 0],
        [0, 1, 0],
        [0, 1, 0],
    ]

    AUTHORITARIAN_2 = [
        [1, 0, 1],
        [0, 1, 1],
        [0, 0, 1],
    ]

    AUTHORITARIAN_2_COLLAPSED = [
        [0, 0, 1],
        [0, 0, 1],
        [0, 0, 1],
    ]

    TRIBAL_0 = [
        [1, 1, 0],
        [0, 1, 1],
        [1, 0, 1],
    ]

    TRIBAL_1 = [
        [1, 0, 1],
        [1, 1, 0],
        [0, 1, 1],
    ]

    COMMUNITARIAN = [
        [1, 1, 1],
        [1, 1, 1],
        [1, 1, 1],
    ]


class RelationalMatrix_4:
    SURVIVALIST = [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1],
    ]

    AUTHORITARIAN_0 = [
        [1, 0, 0, 0],
        [1, 1, 0, 0],
        [1, 0, 1, 0],
        [1, 0, 0, 1],
    ]

    AUTHORITARIAN_0_COLLAPSED = [
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [1, 0, 0, 0],
    ]

    AUTHORITARIAN_1 = [
        [1, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 1, 1, 0],
        [0, 1, 0, 1],
    ]

    AUTHORITARIAN_1_COLLAPSED = [
        [0, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 1, 0, 0],
    ]

    AUTHORITARIAN_2 = [
        [1, 0, 1, 0],
        [0, 1, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 1],
    ]

    AUTHORITARIAN_2_COLLAPSED = [
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
    ]

    AUTHORITARIAN_3 = [
        [1, 0, 0, 1],
        [0, 1, 0, 1],
        [0, 0, 1, 1],
        [0, 0, 0, 1],
    ]

    AUTHORITARIAN_3_COLLAPSED = [
        [0, 0, 0, 1],
        [0, 0, 0, 1],
        [0, 0, 0, 1],
        [0, 0, 0, 1],
    ]
    TRIBAL_0 = [
        [1, 1, 0, 0],
        [0, 1, 1, 0],
        [0, 0, 1, 1],
        [1, 0, 0, 1],
    ]

    TRIBAL_1 = [
        [1, 0, 1, 0],
        [0, 1, 0, 1],
        [1, 0, 1, 0],
        [0, 1, 0, 1],
    ]

    TRIBAL_2 = [
        [1, 0, 0, 1],
        [1, 1, 0, 0],
        [0, 1, 1, 0],
        [0, 0, 1, 1],
    ]

    COMMUNITARIAN = [
        [1, 1, 1, 1],
        [1, 1, 1, 1],
        [1, 1, 1, 1],
        [1, 1, 1, 1],
    ]


if __name__ == "__main__":
    rl_2 = RelationalMatrix_2()
    print(help(rl_2))
