"""
script to receive an array of adjacency matrices and visualize the network evolution.
"""
import cv2
import networkx as nx
import matplotlib.pyplot as plt
import os


class NetworkVis:

    # def get_weighted_G_from_A(self, A):

    def frames_to_vid(self, A, out_path):
        """
        receive an array of adjacency matrices, turn each into an nx network,
        with weights as dark as edges. output a video of network evolution.
        """
        fps = 10
        vid_size = 1024
        out = cv2.VideoWriter(f'{out_path}.mp4', cv2.VideoWriter_fourcc(*'mp4v'), fps, (vid_size, vid_size))
        G = nx.from_numpy_matrix(A[0], create_using=nx.MultiDiGraph())
        pos = nx.spring_layout(G)
        for i in range(A.shape[0]):
            G = nx.from_numpy_matrix(A[i], create_using=nx.DiGraph())
            weights = [G[u][v]['weight'] * 8 for u, v in G.edges()]
            nx.draw(G, pos, width=weights, with_labels=True, node_size=1000)
            plt.savefig("temp.png", dpi=300)
            frame = cv2.imread("temp.png")
            frame = cv2.resize(frame, (vid_size, vid_size), interpolation=cv2.INTER_AREA)
            out.write(frame)
            plt.clf()
        out.release()
        os.remove("temp.png")
