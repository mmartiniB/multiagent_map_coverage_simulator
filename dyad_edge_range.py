"""
script to run simulations for a dyad with edge weights randing from 0 to 1 in 0.1 increments
"""

from multiagent_search_env import MultiAgentSearchEnv
from utils.simulation_util import get_result_row, output_results
from search_grid import SearchGrid
from agents import GRW
import numpy as np
from tqdm import tqdm
import os


if __name__ == '__main__':
    import argparse
    description = """
    simulate a multiagent grid coverage using an input numpy adjacency matrix.
    get simulation results as a csv file for all agents' performance and final search time
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--env_type', type=int, default=0,
                        help="type of environment - 0: MultiAgentSearchEnv")
    parser.add_argument('--agents_types', type=int, default=(0,), nargs="+",
                        help="agents' algorithms - 0: Guided Random Walk")
    parser.add_argument('--fovs', type=int, default=(1,), nargs="+",
                        help="agents' fields of view in Manhattan distance")
    parser.add_argument('--agents_periods', type=int, nargs="+", default=(1,),
                        help="tuple of agents periods (timesteps/cell)")
    parser.add_argument('--networks_dir', type=str,
                        help="path to .npy square adjacency matrices")
    parser.add_argument('--grid_path', type=str, default="maps/empty_10.npy",
                        help='path to image or npy search map')
    parser.add_argument('--n_rounds', type=int, default=1,
                        help='number of rounds played for each condition')
    parser.add_argument('--out_path', type=str, default="results/f_database_{}.csv",
                        help='path of output csv file')
    args = parser.parse_args()

    n_agents = 2

    # get agents types (indexes)
    agents_types = list(args.agents_types)
    if len(agents_types) != n_agents:
        agents_types = np.random.choice(agents_types, size=(n_agents,))

    # handle agents periods - default to same speeds
    agents_periods = list(args.agents_periods)
    if len(agents_periods) != n_agents:
        agents_periods = np.random.choice(
            agents_periods, size=(n_agents,))

    # handle agents' fovs
    fovs = list(args.fovs)
    if len(fovs) != n_agents:
        fovs = np.random.choice(fovs, size=(n_agents,))

    # path to all adjacency matrices
    networks_dir = args.networks_dir

    # number of rounds per condition
    n_rounds = args.n_rounds

    # search grid
    search_grid = SearchGrid(args.grid_path)

    # prepare agents
    AGENTS_CLASSES = [
        GRW,  # id_: int, period: int, rel_row:np.array, fov: int,
    ]
    # prepare envirnoment
    ENVS = [
        MultiAgentSearchEnv,  # search_grid: np.array, agents: obj, vis: bool
    ]

    # output csv file
    out_path = args.out_path.format(n_agents)

    networks_names = [name for name in list(
        os.listdir(networks_dir)) if name.endswith("npy")]

    for network_name in tqdm(networks_names):

        # create adjacency matrix
        network_path = os.path.join(networks_dir, network_name)
        adj_matrix = np.load(network_path)

        # create agents
        AGENTS_ARGS = [
            lambda i: (i, agents_periods[i], adj_matrix[i], fovs[i]),
        ]
        agents = [AGENTS_CLASSES[agent_type](*AGENTS_ARGS[agent_type](
            i)) for i, agent_type in enumerate(agents_types)]

        # create environment
        ENVS_ARGS = [
            (search_grid, agents),
        ]
        env = ENVS[args.env_type](*ENVS_ARGS[args.env_type])

        # run the search rounds
        results = []
        for round_idx in range(args.n_rounds):
            env.reset()
            env.search()

            result_row = get_result_row(
                env, agents_types, fovs, agents_periods, network_path, args.grid_path)
            results.append(result_row)
        output_results(results, n_agents, args.out_path.format(n_agents))
