import numpy as np
import cv2
"""
class to define the class for the seach map
search map object contains a 2d array of cell objects
"""

EMPTY = 0
WALL = 1


def is_padded(search_map: np.array):
    """return if the image array is padded with ones"""
    return np.all(search_map[0]) and np.all(search_map[-1]) and np.all(search_map[:, 0]) and np.all(search_map[:, -1])


def ensure_padding(search_map: np.array):
    """pad the search map with walls if not padded already"""
    if not is_padded(search_map):
        search_map = np.pad(search_map, (1, 1),
                            mode="constant", constant_values=(WALL))
    return search_map


class Cell:
    def __init__(self, pos):
        self.visitors = []  # list of agents who have visited the cell
        self.notified = dict()  # list of agents who recognize the marking of that cell
        self.pos, (self.row, self.col) = pos, pos
        self.actions = []  # valid actions in that cell
        self.neighbors = []

    def __repr__(self):
        return f"({self.row}, {self.col})"


class SearchGrid:
    def __init__(self, search_grid_path) -> None:
        """
        given the path, know if its a numpy array or an image.
        read the grid and make a cells grid out of it 
        """
        self.right, self.down, self.left, self.up = (
            0, 1), (1, 0), (0, -1), (-1, 0)
        self.actions = [self.right, self.up, self.left, self.down]
        self.num_actions = len(self.actions)

        grid = self.read_grid_file(search_grid_path)
        grid = ensure_padding(grid)

        self.shape, (self.h, self.w) = grid.shape, grid.shape
        self.empty_rows, self.empty_cols = np.where(grid == EMPTY)
        self.num_cells = len(self.empty_rows)

        self.grid = self.create_cell_grid(grid)

        self.visited_cells = 0

    @staticmethod
    def read_grid_file(filename) -> np.array:
        if filename.endswith("npy"):
            grid = np.load(filename)
        else:
            grid = cv2.imread(filename, 0)
            _, grid = cv2.threshold(grid, 50, 1, cv2.THRESH_BINARY)
        return grid

    def get_valid_actions_neighbors(self, grid, pos):
        """
        return a list of valid ations at a given position valid
        actions avoid the "WALL" cells (obstacles and boundary)
        """
        valid_actions = []
        valid_neighbors = []
        for action in self.actions:
            pos_next = pos[0] + action[0], pos[1] + action[1]
            if grid[pos_next] != WALL:
                valid_actions.append(action)
                valid_neighbors.append(pos_next)
        return valid_actions, valid_neighbors

    def create_cell_grid(self, grid) -> np.array:
        """
        at each empty position, create a cell object, and 
        populate its row, col and actions attributes
        """
        cells_grid = np.empty(grid.shape, Cell)
        for i in range(grid.shape[0]):
            for j in range(grid.shape[1]):
                pos = (i, j)

                # ignore walls
                if grid[pos] == WALL:
                    continue

                cells_grid[pos] = Cell(pos)
                cells_grid[pos].actions, cells_grid[pos].neighbors = self.get_valid_actions_neighbors(
                    grid, pos)

        return cells_grid

    def reset_grid(self):
        for row in self.grid:
            for cell in row:
                if isinstance(cell, Cell):
                    cell.visitors = []
                    cell.notified = dict()

    def reset(self) -> None:
        self.reset_grid()
        self.visited_cells = 0
