from random import choice, sample
import numpy as np
from search_grid import Cell, SearchGrid


class Agent:
    def __init__(self, id_: int, period: int):
        self.id = id_
        self.period = period
        self.v = 1 / period

        self.search_time = 0

    def update_cell(self, cell):
        # add notified agents to cell according to adj_mat
        for other_id, marking in enumerate(self.rel_row):
            rand = np.random.choice((True, False), p=(marking, 1 - marking))
            if rand:
                cell.notified[other_id] = 1

    def __repr__(self):
        return f"Agent {self.id}"


class GRW(Agent):
    def __init__(self,
                 id_: int,
                 period: int,
                 rel_row,
                 fov=1,
                 ):
        super(GRW, self).__init__(id_, period)
        self.right, self.down, self.left, self.up = (
            0, 1), (1, 0), (0, -1), (-1, 0)
        self.actions = [self.right, self.up, self.left, self.down]

        # column where agents are indices and the value is the probability
        # that the agent will mark for them
        self.rel_row = rel_row

        self.fov = fov  # side length of observable square around the agent
        self.pos = None

    def action_to_step(self, pos, action):
        row, col = pos
        dr, dc = action
        return row + dr, col + dc

    def observable(self, cell1, cell2):
        dy = abs(cell1[0] - cell2[0])
        dx = abs(cell1[1] - cell2[1])
        return (dx + dy) <= self.fov

    def bfs(self, search_grid, pos):
        que = [pos]
        vis_grid = np.zeros_like(search_grid.grid, dtype=bool)
        vis_grid[pos] = 1
        while len(que) > 0:
            # if self.id == 0:
            #     print(que)
            curr_pos = que.pop(0)
            surr_steps = search_grid.grid[curr_pos].neighbors
            for surr_step in sample(surr_steps, len(surr_steps)):
                if vis_grid[surr_step] == 1:
                    # if step was already added to the queue
                    continue
                if not self.observable(pos, surr_step):
                    # if cell outside nxn square
                    continue
                if search_grid.grid[surr_step].notified.get(self.id):
                    que.append(surr_step)
                    vis_grid[surr_step] = 1
                else:
                    return surr_step
        return None

    def get_dir(self, source, dest):
        # get the direction that brings the agent closer to the destination
        actions = self.actions.copy()
        row_s, col_s = source
        row_d, col_d = dest
        if row_d <= row_s:
            actions.remove(self.down)
        if row_d >= row_s:
            actions.remove(self.up)

        if col_d <= col_s:
            actions.remove(self.right)
        if col_d >= col_s:
            actions.remove(self.left)

        action = choice(actions)
        return action

    def get_initial_pos(self, search_grid):
        idx = choice(range(search_grid.num_cells))
        return search_grid.empty_rows[idx], search_grid.empty_cols[idx]

    def choose_action(self, search_grid):
        update = False
        if self.pos is None:
            # the first step
            self.pos = self.get_initial_pos(search_grid)

        # handle movement action
        pos_empty = self.bfs(search_grid, self.pos)
        cell = search_grid.grid[self.pos]
        if pos_empty is None:
            pos_next = choice(cell.neighbors)
        else:
            action = self.get_dir(source=self.pos, dest=pos_empty)
            if action not in cell.actions:
                action = choice(cell.actions)
            pos_next = self.pos[0] + action[0], self.pos[1] + action[1]
        self.pos = pos_next

        # handle marking action
        cell_next = search_grid.grid[pos_next]
        if not cell_next.notified.get(self.id):
            # if you've never visited the cell before, add yourself to the visitors
            cell_next.visitors.append(self.id)
            update = True
        self.update_cell(cell_next)

        return cell_next, update
