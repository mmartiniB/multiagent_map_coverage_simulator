"""
script to generate a csv file containing the simulation results for FOV vs. Network Density

1- generate all adjacency matrices of N nodes
2- generate all test FOVs
3- simulate each (matrix, fov) pair for k times and add results to dataframe
"""

from multiagent_search_env import MultiAgentSearchEnv
from utils.simulation_util import get_result_row, output_results
from search_grid import SearchGrid
from agents import GRW
import numpy as np
from tqdm import tqdm
import os


if __name__ == '__main__':
    import argparse
    description = """
    simulate a multiagent grid coverage using an input numpy adjacency matrix.
    get simulation results as a csv file for all agents' performance and final search time
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--n_agents', type=int, help="number of agents")
    parser.add_argument('--fov_min', type=int, default=1, help="minimum fov")
    parser.add_argument('--fov_max', type=int, default=1, help="maximum fov")
    parser.add_argument('--fov_inc', type=int, default=1, help="fov increment")
    parser.add_argument('--networks_dir', type=str,
                        help="path to .npy square adjacency matrices")
    parser.add_argument('--grid_path', type=str, default="maps/empty.npy",
                        help='path to image or npy search map')
    parser.add_argument('--n_rounds', type=int, default=1,
                        help='number of rounds played for each condition')
    parser.add_argument('--out_path', type=str, default="results/database_{}.csv",
                        help='path of output csv file')
    args = parser.parse_args()

    n_agents = args.n_agents
    agents_types = [0] * n_agents
    AGENTS_PERIODS = np.ones(n_agents, dtype=int)

    fovs = list(range(args.fov_min, args.fov_max + 1, args.fov_inc))
    networks_dir = args.networks_dir

    n_rounds = args.n_rounds
    grid_path = args.grid_path
    search_grid = SearchGrid(grid_path)
    out_path = args.out_path.format(n_agents)

    networks_names = [name for name in list(
        os.listdir(networks_dir)) if name.endswith("npy")]

    for network_name in tqdm(networks_names):
        if not network_name.endswith("npy"):
            continue
        network_path = os.path.join(networks_dir, network_name)
        adj_matrix = np.load(network_path)

        for fov in fovs:

            # create agents
            agents = [GRW(i, AGENTS_PERIODS[i], adj_matrix[i], fov)
                      for i in range(n_agents)]

            # create environment
            env = MultiAgentSearchEnv(search_grid, agents)

            results = []
            for round in range(n_rounds):
                env.reset()
                env.search()
                results.append(get_result_row(env, agents_types,
                               [fov] * n_agents, AGENTS_PERIODS, network_path, grid_path))
            output_results(results, n_agents, out_path)
